#https://github.com/CentOS/CentOS-Dockerfiles
#https://github.com/CentOS/CentOS-Dockerfiles/tree/master/httpd/centos7
FROM centos:latest as production
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor="COTEC"
LABEL Description="Centos 7 , PHP 7.2, composer"
LABEL License=GPLv2
LABEL Version=0.0.1
LABEL Features="1 melhoria x meria y"
LABEL tag=""

#RUN yum --setopt=tsflags=nodocs update -y
#RUN yum  clean all
RUN yum --setopt=tsflags=nodocs install epel-release -y && \
 yum --setopt=tsflags=nodocs install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y  && \
 yum --setopt=tsflags=nodocs install yum-utils -y  && \
 yum-config-manager --enable remi-php72  && \
 yum --setopt=tsflags=nodocs install php php-fpm php-pgsql php-gd php-json php-mbstring php-mysqlnd php-xml php-xmlrpc php-opcache php-pecl-crypto php-pdo php-common   httpd -y && \
 yum --setopt=tsflags=nodocs install -y php-zipstream php-pecl-zip php-pclzip  wget unzip zip libpng-devel && \
 yum clean all && \
 rm -rf /var/cache/yum
#RUN systemctl start httpd   => erro
#RUN systemctl enable httpd  => erro

# Instalacao do composer
RUN curl -sS https://getcomposer.org/installer | php  && \
 mv composer.phar /usr/local/bin/composer

ENV APP_HOME=${APP_HOME:-/var/www/html}
WORKDIR $APP_HOME

#ADD conf/etc/php.ini /etc/php.ini
#ADD conf/etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf

COPY src/  $APP_HOME

RUN composer install --no-dev --optimize-autoloader

EXPOSE 443 80 8000 5001

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

RUN mv /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/welcome.conf.old

ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh

CMD ["/run-httpd.sh"]


FROM production as testing
#MAINTAINER ICMBIO <roquebrasilia@gmail.com>
LABEL Vendor="COTEC"
LABEL Description="Centos 7 , PHP 7.2, composer , npm,  yarn"
LABEL License=GPLv2
LABEL Version=0.0.1
ENV PDCI_SONARQUBE_URL_PORT=${PDCI_SONARQUBE_URL_PORT:-http://localhost:9000}
# Instalacao do node para os testes
RUN curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -  && \
     yum install -y --setopt=tsflags=nodocs lynx net-tools git maven ant wget automake autoconf  dh-autoreconf nasm libjpeg-turbo-utils  php-pecl-xdebug  php-cli  pngquant nodejs && \
     yum clean all && \
     rm -rf /var/cache/yum && \
     npm  install -g  yarn

WORKDIR /opt/

RUN wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip && \
    unzip sonar-scanner-cli-3.2.0.1227-linux.zip && \
    rm -f sonar-scanner-cli-3.2.0.1227-linux.zip && \
    export PATH="$PATH:/opt/sonar-scanner-3.2.0.1227-linux/bin"

COPY conf/sonar-scanner.properties  /opt/sonar-scanner-3.2.0.1227-linux/conf
COPY scripts/pdci_run_tests.sh /bin/pdci_run_tests.sh
RUN chmod +x /bin/pdci_run_tests.sh

RUN ln -s /opt/sonar-scanner-3.2.0.1227-linux/bin/sonar-scanner /bin/sonar-scanner



WORKDIR $APP_HOME

RUN composer install && \
  yarn install

CMD ["pdci_run_tests.sh"]